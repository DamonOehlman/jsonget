
// req: superagent

var isBrowser = typeof window != 'undefined',
    debug = isBrowser ? function() {} : require('debug')('jsonget'),
    callbackCounter = new Date().getTime(),
    reStatusOK = /^(?:2|3)\d*$/,
    head;
    
// TODO: make this work
function noCORS(url) {
    // typeof XMLHttpRequest != 'undefined' && !('withCredentials' in new XMLHttpRequest()),
    
    return true;
}

function jsonp(url, data, opts, callback) {
    // create the url args
    var args = [], script, done = false;
    
    // iterate through the data and create key value pairs
    for (var key in data) {
        args[args.length] = escape(key) + '=' + escape(data[key]);
    }
    
    // apply either a ? or & to the url depending on whether we already have query params
    url += (url.indexOf('?') >= 0 ? '&' : '?') + opts.cbParam + '=' + opts.fnname + '&';
    
    // if the callback function does not exist, then create it
    if (! window[fnName]) {
        // create the callback function
        window[fnName] = function(data) {
            // trigger the callback
            callback(null, data);

            // delete this function
            window[fnName] = undefined;
        };
    }
    
    // create the loader script
    script = document.createElement('script');
    
    // initialise the script
    script.src = url + args.join('&');
    script.async = true;
    
    // handle the script loading
    script.onload = script.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')) {
            done = true;
            script.onload = script.onreadystatechange = null;
            if (script && script.parentNode) {
                script.parentNode.removeChild(script);
            }
        }
    };
    
    // if we don't have a reference to the head section, then do that now
    if (! head) {
        head = document.getElementsByTagName('head')[0];
    }
    
    // add the script tag
    head.appendChild(script);
}

function makeRequest(url, data, opts, callback) {
    // if we have the force callback param, clone the data and add a callback
    if (opts.forceCallback) {
        var clonedData = {};
        
        for (var key in data) {
            clonedData[key] = data[key];
        }
        
        data = clonedData;
        data[opts.cbParam] = 'fakecb';
    }
    
    superagent
        .get(url)
        .send(data)
        .end(function(res) {
            var err;
            
            // if the status code is not ok, then update the err
            if (! reStatusOK.test(res.status)) {
                err = new Error('Not OK (status = ' + res.status + ')');
            }
            
            // if we are in force callback mode, then extract the actual data
            if ((!err) && opts.forceCallback) {
                var text = res.text.slice(res.text.indexOf('(') + 1, res.text.lastIndexOf(')'));

                // attempt to parse the response body
                try {
                    res.body = JSON.parse(text);
                }
                catch (e) {
                    err = new Error('Unable to parse data using forced callback');
                }
            }
            
            callback(err, res.body);
        });
}

/**
# jsonget
This is the main function of the jsonget helper and behind the scenes will
invoke either a JSONP style request or a standard XHR (or server-side) request
if acceptable.

## Valid Options

- fnname:         The JSONP callback function name (default = json%counter%)
- cbParam:        The JSONP callback parameter passed to the JSONP compatible endpoint (default = callback)
- forceCallback:  Some (broken) webservices will only respond if a callback parameter is supplied.  Use this option if working with
                  one of those services and you are doing server side integration in addition to browser.

*/
function jsonget(url, data, opts, callback) {
    // remap args if required
    if (typeof opts == 'function') {
        callback = opts;
        opts = {};
    }
    
    // ensure we have options
    opts = opts || {};
    opts.fnname = opts.fnname || 'json' + (callbackCounter++);
    opts.cbParam = opts.cbParam || 'callback';
    
    // if jsonp is required, then used the script loader
    if (isBrowser && noCORS(url)) {
        jsonp(url, data, opts, callback);
    }
    // otherwise, use superagent
    else {
        makeRequest(url, data, opts, callback);
    }
}