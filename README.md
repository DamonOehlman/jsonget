# jsonget

This is a very simply library that wraps superagent to provide JSONP support where required.  The idea being that regardless of your environment, server, CORS-compliant browser, old broken browser, you can call `jsonget` and get the data you need.

As per the name it is built to handle `GET` requests only.  Primarily, because if you are using JSONP you ain't going to be using anything but.

## Example Usage

Geonames example:

```js
jsonget('http://api.geonames.org/searchJSON', { q: 'brisbane', maxRows: 10, lang: 'en', username: 'demo' }, function(err, data) {
    // do something
});
```

## NOTES

At this stage jsonget does not have any error detection for a failed script load, and in fact all that probably will be possible is to monitor for timeout errors.  This will be investigated though.
