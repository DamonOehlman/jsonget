describe('validate jsonget works against geonames services', function() {
    var jsonget = require('../dist/commonjs/jsonget'),
        expect = require('expect.js'),
        searchOpts = {
            q: 'brisbane', 
            maxRows: 10, 
            lang: 'en', 
            username: 'DamonOehlman'
        };

    it('should be able to perform a geonames search', function(done) {
        jsonget('http://api.geonames.org/searchJSON', searchOpts, function(err, data) {
            expect(err).to.not.be.ok();
            
            expect(data).to.be.ok();
            expect(data.totalResultsCount).to.be.above(0);
            
            done(err);
        });
    });
    
    it('should be able to perform a search, with a forced callback param', function(done) {
        jsonget('http://api.geonames.org/searchJSON', searchOpts, { forceCallback: true }, function(err, data, res) {
            expect(err).to.not.be.ok();

            expect(data).to.be.ok();
            expect(data.totalResultsCount).to.be.above(0);

            // check that we didn't come via a proxy
            expect(res.headers.via).to.not.be.ok();
            
            done(err);
        });
    });

    it('should be able to perform a search via a proxy (ignored on the client)', function(done) {
        jsonget('http://api.geonames.org/searchJSON', searchOpts, { proxy: 'http://107.20.247.122:8888' }, function(err, data, res) {
            expect(err).to.not.be.ok();

            expect(data).to.be.ok();
            expect(data.totalResultsCount).to.be.above(0);

            // check that we did come via a proxy
            expect(res.headers.via).to.be.ok();            
            
            done(err);
        });
    });
});